<?php
function varnish_manual_clear($form_id, $form=FALSE, $form_state=FALSE){

  $form['varnish_manual_clear_css_js'] = array(
  
    '#type' => 'fieldset',
    '#title' => t('Clear CSS & JS'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  
   $form['varnish_manual_clear_css_js']['varnish_manual_clear_css_js_selection'] = array(
  
    '#type' => 'select',
    '#title' => t('What to clear?'),
    '#required' => FALSE,
    '#options' => array('' => 'Select...', 'all'=>'All', 'css'=>'CSS', 'js'=>'JS')
  );
  
  
  $form['varnish_manual_clear_css_js']['varnish_manual_clear_css_js_submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Clear JS and/or CSS'),
  );
  
  
  $menu_name = variable_get('menu_primary_links_source', 'primary-links');
  $links = menu_tree_page_data($menu_name);
  
  foreach($links as $id => $item) {
  
    $path = drupal_get_path_alias($item['link']['link_path']);
    $paths[] = $path;
  }

  if (is_array($paths)){
  
    $form['varnish_manual_clear_primary'] = array(
    
      '#type' => 'fieldset',
      '#title' => t('Clear Primary Menu Paths'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#description' => t('Clears Varnish for paths starting in the selected primary paths')
    );
    
     $form['varnish_manual_clear_primary']['varnish_manual_clear_primary_selection'] = array(
    
      '#type' => 'select',
      '#title' => t('What to clear?'),
      '#required' => FALSE,
      '#multiple' => TRUE,
      '#options' => drupal_map_assoc($paths)
    );
    
    
    $form['varnish_manual_clear_primary']['varnish_manual_clear_primary_submit'] = array(
      '#type' => 'submit', 
      '#value' => t('Clear from Primary menu root'),
    );
  }
  $form['varnish_manual_clear_assets'] = array(
  
    '#type' => 'fieldset',
    '#title' => t('Clear Assets'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#description' => t('Clears Varnish for the selected files extensions')
  );
  
   $form['varnish_manual_clear_assets']['varnish_manual_clear_assets_selection'] = array(
  
    '#type' => 'select',
    '#title' => t('What to clear?'),
    '#required' => FALSE,
    '#multiple' => TRUE,
    '#options' => array_merge(array('all'=>'All'), drupal_map_assoc(array('png','gif','jpg','jpef','swf','flv','f4v','css','js','mov','mp3','mp4','pdf','doc','ttf','eot','ppt','zip','ico','ogg','wmv','dmg','dll','bmp')))
  );

  $form['varnish_manual_clear_assets']['varnish_manual_clear_assets_submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Clear selected assets')
  );

  $form['varnish_manual_clear_html'] = array(
  
    '#type' => 'fieldset',
    '#title' => t('Clear HTML'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#description' => t('All URLs without extensions')
  );
  
  $form['varnish_manual_clear_html']['varnish_manual_clear_html_submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Clear all HTML')
  );

  $form['varnish_manual_clear_all'] = array(
  
    '#type' => 'fieldset',
    '#title' => t('Clear Everything'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  
  $form['varnish_manual_clear_all']['varnish_manual_clear_all_submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Clear everything')
  );
  
  return $form;
}

function varnish_manual_clear_validate($form, &$form_state) {
  
  switch($form_state['values']['op']){
  
    case 'Clear JS and/or CSS':
    
      if (empty($form_state['values']['varnish_manual_clear_css_js_selection'])){
  
        form_set_error('varnish_manual_clear_css_js_selection', 'Value must be selected', $reset = FALSE);
      }
      
      break;
  
    case 'Clear from Primary menu root':
    
      if (empty($form_state['values']['varnish_manual_clear_primary_selection'])){
  
        form_set_error('varnish_manual_clear_primary_selection', 'Value must be selected', $reset = FALSE);
      }
      
      break;
  }
}

function varnish_manual_clear_submit($form, &$form_state){
  

  switch($form_state['values']['op']){
  
    case 'Clear JS and/or CSS':
    
      $value = $form_state['values']['varnish_manual_clear_css_js_selection'];
      
      if ($value == 'all') $pattern = '.*.(js|css)';
      else $pattern = ".*.$value";
        
      break;
  
    case 'Clear from Primary menu root':
    
      $value = $form_state['values']['varnish_manual_clear_primary_selection'];
      
      
      $pattern = '('.implode('|', str_replace('<front>', '$', $value)).')';
      
      break;
  
    case 'Clear selected assets':
          
      $value = $form_state['values']['varnish_manual_clear_assets_selection'];
      $all_values = array_keys($form['varnish_manual_clear_assets']['varnish_manual_clear_assets_selection']['#options']);
      unset($all_values['all']);
      
      if (array_key_exists('all', $value)) $value = $all_values;
      $pattern = '.*.('.implode('|', $value).')';
      break;
  
    case 'Clear all HTML':
      
      $all_values = array_merge(  
                      array('css','js'), 
                      $form['varnish_manual_clear_assets']['varnish_manual_clear_assets_selection']['#options']);
                      
    
      unset($all_values['all']);
      
      
      //$pattern = '.*(.[a-zA-Z]{2,4})+';
      $pattern = '.*.('.implode('|', $all_values).')';
      $pattern_negative = TRUE;
      break;
  
    case 'Clear everything':
      
      $pattern = '';
      break;
  }
  
  $pattern = _varnish_manual_purge($pattern, $pattern_negative);
  drupal_set_message('Varnish cleared: '.$form_state['values']['op'], 'status');
  drupal_set_message('Pattern: '.htmlentities($pattern), 'status');
}


function _varnish_manual_purge($path, $pattern_negative=FALSE) {

  $path = base_path().$path;
  $host = _varnish_get_host();
  
  if ($pattern_negative) $pattern_negative_token = '!';
  
  $pattern = "purge req.http.host ~ {$host} && req.url {$pattern_negative_token}~ ^{$path}";
  _varnish_terminal_run($pattern);
  
  return $pattern;
}

